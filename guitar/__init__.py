"""
guitar Module
"""
from flask import Flask
from flask import current_app
from flask_restful import Api
from flask_cors import CORS
from flasgger import Swagger


from .V1.guitar_tech import GuitarTuner
from .V1.guitar_tech import GuitarRestringer
from .V1.guitar_tech import GuitarFinder
from .V1.guitar_tech import GuitarLister
from .V1.guitar_tech import MakerLister
from .V1.guitar_tech import AddStringMaker
from .V1.guitar_tech import AddStringSet
from .V1.guitar_tech import AddStringType

app = Flask(__name__)
app.config.from_object('config')
Swagger(app)
CORS(app)


api = Api(app)
api.add_resource(GuitarTuner, '/tuner', methods=['PUT'])
api.add_resource(GuitarRestringer, '/restring')
api.add_resource(GuitarFinder, '/guitarfinder')
api.add_resource(GuitarLister, '/guitars')
api.add_resource(MakerLister, '/makers')
api.add_resource(AddStringMaker, '/addstringmaker', methods=['POST'])
api.add_resource(AddStringSet, '/addstringset', methods=['POST'])
api.add_resource(AddStringType, '/addstringtype', methods=['POST'])

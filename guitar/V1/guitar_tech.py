"""
guitar_tech is a module to manage and work on guitars for players
"""

from datetime import datetime, date
from flask import current_app
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from sqlalchemy import update
from flasgger.utils import swag_from

from db_manager.database import Session

from flask_restful import Resource
from guitar.models import Guitar
from guitar.models import StringMaker
from guitar.models import StringSet
from guitar.models import Maker
from guitar.models import StringType

from guitar.models import model_schema
from guitar.models import maker_schema
from sqlalchemy import exc

def _get_guitar(sn, maker):
    session = Session()
    m = session.query(Maker).filter_by(name=maker).first()
    guitar_obj = session.query(Guitar).filter_by(serial_number = sn,
                                        maker_id = m.id).first()

    return guitar_obj

def _get_date_time():
    dt = datetime.now()
    return dt

class MakerLister(Resource):
    """ class to provide a list of guitar makers """
    def get(self):
        session = Session()
        makers = session.query(Maker).all()
        # makers = session.query(Maker.name, Maker.models).all()
        current_app.logger.debug("Makers are: %s", makers)
        lor = list()

        for maker in makers:
            current_app.logger.debug("maker is: {}".format(maker))
            current_app.logger.debug("maker ID is: {}".format(maker.id))
            #models = maker.models
            '''
            m = {'id': maker.id,
                 'make': maker.name,
                 'models': model_schema.dump(maker.models)}
            '''
            data, errors = maker_schema.dump(maker)
            current_app.logger.debug("record is: {}".format(data))
            lor.append(data)

        current_app.logger.debug("list of records are: {}".format(lor))

        return lor
        
class GuitarLister(Resource):
    """ class to provide tech work details """
    @swag_from('guitarlister.yml')
    def get(self):

        """
        
    
        """
        session = Session()
        
        guit = session.query(Guitar).all()
        current_app.logger.debug("Guitar is: %s", guit)
        
        lor = list()
        pl = dict()
        
        for g in guit:
            current_app.logger.debug("guit is: %s", g)
            
            pl['id'] = g.id
            pl['name'] = g.name
            pl['maker_id'] = g.maker_id
            pl['model_id'] = g.model_id
            pl['last_tuned'] = str(g.last_tuned)
            pl['last_restring'] = str(g.last_restring)
            pl['string_gauge'] = g.string_gauge
            pl['string_brand'] = g.string_brand
               
        return pl
    
                                
class GuitarFinder(Resource):
    """ class to provide tech work details """


    api_args = {
        'serial_number': fields.Str(required=True),
        'maker': fields.Str(required=True)}

    @use_kwargs(api_args)
    @swag_from('guitarfinder.yml')    
    def get(self, serial_number, maker):
        """ lookup a guitar by serial number """

        session = Session()

        m = session.query(Maker).filter_by(name=maker).first()        
        g = session.query(Guitar).filter_by(serial_number = serial_number,
                                            maker_id = m.id).first()
        current_app.logger.debug("Guitar is: %s", g)
        
        if g is None:
            return "No guitar found for s/n: {}".format(self.serial_number)

        pl = dict()
        pl['id'] = g.id
        pl['name'] = g.name
        pl['maker_id'] = g.maker_id
        pl['model'] = g.model_id
        
        return pl
    
class GuitarTuner(Resource):
    """ class to provide tech work details """
    api_args = {
        'serial_number': fields.Str(required=True),
        'maker': fields.Str(required=True),
        'tuning': fields.Str()} 
        
    @use_kwargs(api_args)
    def put(self, serial_number, maker, tuning):
        session = Session()
        # get the guitar id
        
        # set datetime
        # set guitar last tuned attribute 
        # set guitar tuned to specific tuning
        current_app.logger.debug("Got PUT tuning")

        gtr = _get_guitar(serial_number, maker)
        if gtr is None:
            return "While tryning to tune the guitar, we cannot find the guitar: {}".format(serial_number)
        current_app.logger.debug("Got guitar id: %s", gtr.id)
        dtn = datetime.now()
        current_app.logger.debug("DT now : %s", dtn)
        m = session.query(Maker).filter_by(name=maker).first()
        g = session.query(Guitar).filter_by(serial_number = serial_number,
                                            maker_id = m.id).update({'last_tuned': dtn})
        
        session.commit()
        return "tuning guitar, eh? Tuned guitar id: {} on: {}".format(g.name,dtn)

class AddStringMaker(Resource):
    """ class to provide tech worker the ability to add a new string maker """
    api_args = {
        'maker_name': fields.Str(required=True)} 
    @use_kwargs(api_args)
    def post(self, maker_name):
        session = Session()
        current_app.logger.debug("Got PUT addstringmaker")
        string_maker = StringMaker(maker_name=maker_name)
        m = session.add(string_maker)
        session.commit()
        return "Added string maker: {}".format(maker_name)

class AddStringSet(Resource):
    """ class to provide tech worker the ability to add a new string maker-model """
    api_args = {
        'set_name': fields.Str(required=True),
        'string_maker_id': fields.Int(required=True),
        } 
    @use_kwargs(api_args)
    def post(self, set_name, string_maker_id):
        session = Session()
        current_app.logger.debug("Got POST addstringmaker-model")
        string_set = StringSet(set_name=set_name, string_maker_id=string_maker_id)
        string_maker = session.query(StringMaker).filter_by(id=string_maker_id).first()
        try:
            session.add(string_set)
            session.commit()
        except exc.IntegrityError as db_err:
            current_app.logger.debug("Got error: {}".format(db_err) )
            current_app.logger.error("Got sql integrity error".format(db_err))
            session.rollback()
            return "Added string maker:{}, set:{} -> ERROR: {}".format(string_maker.maker_name, set_name, db_err)
        return "Added string maker:{}, set:{}".format(string_maker.maker_name, set_name)

class AddStringType(Resource):
    """ class to provide tech worker the ability to add a new string type """
    api_args = {
        'type_name': fields.Str(required=True)
        } 
    @use_kwargs(api_args)
    def post(self, type_name ):
        session = Session()
        try:
            string_type = StringType(string_type_name=type_name)
            session.add(string_type)
        except Exception as err:
            error_msg = "got string type error: {}".format(str(err))
            current_app.logger.error("got string type error: {}".format(str(err)))
            return "Got error as: {}".format(error_msg)
        session.commit()
        return "Added string type: {}".format(type_name)
    

class GuitarRestringer(Resource):
    """ guitar tech class to restring a guitar"""
    
    api_args = {
        'serial_number': fields.Str(required=True),
        'maker': fields.Str(required=True),
        'string_gauge': fields.Str()} 
        
    @use_kwargs(api_args)
    def put(self, serial_number, maker, string_gauge):
        session = Session()
        # get the guitar id
        
        # set datetime
        # set guitar last tuned attribute 
        # set guitar tuned to specific tuning
        current_app.logger.debug("Got PUT restring")
        
        dtn = datetime.now()
        current_app.logger.debug("DT now : %s", dtn)
        
        m = session.query(Maker).filter_by(name=maker).first()
        current_app.logger.debug("Maker  id is %s", m.id)
        
        try:
            g = session.query(Guitar).filter_by(serial_number = serial_number,
                                                maker_id = m.id).update({'last_restring': 
                                                                       dtn,
                                                                       'string_gauge':
                                                                       string_gauge})
        
            
            session.commit()
        except Exception as err:
            current_app.logger.error("Got PUT error as: %s", str(err))
        return "Restringing the guitar, eh? With %s" % string_gauge
    '''
    def __init__(self, serial_number):
        self.guitar_sn = serial_number
          
    def put(self, string_set=True, string_gauge='light'):
        """ this API will restring the guitar while setting gauge, restring date.
        
        args are string_set (default true), gauge
        """
        # set datetime
        # set guitar last restring datatime 
        # set guitar string gauge
        guitar = db_session.query(Guitar)
        current_app.logger.debug("Guitar is: %s", guitar)
        payload = dict()
        payload['guitar_id'] = guitar.id
        
        return payload
        pass
    '''

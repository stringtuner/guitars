from sqlalchemy import Table, Column, Integer, String, DateTime, Text
from sqlalchemy import UniqueConstraint
from db_manager.database import Base
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship
from marshmallow import Schema, fields


class Guitar(Base):
    __tablename__ = 'guitar'

    id = Column(Integer, primary_key=True)
    serial_number = Column(String)
    name = Column(String, nullable=True)
    maker_id = Column(Integer, ForeignKey('maker.id'))
    model_id = Column(Integer, ForeignKey('model.id'))
    last_tuned = Column(DateTime)
    last_restring = Column(DateTime)
    string_gauge = Column(String, nullable=True)
    string_brand = Column(String, nullable=True)
       

    def __repr__(self):
        return "<Guitar(name='%s', maker_id='%s', model_id='%s')>" % (
             self.name, self.maker_id, self.model_id)
        

class Maker(Base):
    __tablename__ = 'maker'

    id = Column(Integer, primary_key=True)
    maker_name = Column(String, nullable=True)
    models = relationship('Model', backref='maker')
    
    def __repr__(self):
        return "<Maker(name='%s')>" % (self.name)
        
class Model(Base):
    __tablename__ = 'model'
    
    id = Column(Integer, primary_key=True)
    model_name = Column(String, nullable=True)
    maker_id = Column(Integer, ForeignKey('maker.id'))


association_table = Table('association', Base.metadata,
    Column('string_maker_id', Integer, ForeignKey('string_maker.id')),
    Column('string_model_id', Integer, ForeignKey('string_model.id'))
)
    
class StringChange(Base):
    __tablename__ = 'string_change'

    id = Column(Integer, primary_key=True)
    guitar_id = Column(Integer, ForeignKey('guitar.id'))
    string_type_id = Column(Integer,ForeignKey('string_type.id'))
    string_maker_id = Column(Integer,ForeignKey('string_maker.id'))
    string_model_id = Column(Integer,ForeignKey('string_model.id'))
    comments = Column(Text, nullable=True)
    change_date = Column(DateTime)

    def __repr__(self):
        return "<StringChange(id='%d')>" % (self.id)

class StringMaker(Base):
    __tablename__ = 'string_maker'

    id = Column(Integer, index=True, unique=True, primary_key=True)
    maker_name = Column(String, nullable=True)
    
    def __init__(self, maker_name=None):
        self.maker_name = maker_name
    
    def __repr__(self):
        return "<Maker(name='%s')>" % (self.maker_name)

class StringSet(Base):
    __tablename__ = 'string_set'
    
    string_id = Column(Integer, index=True, unique=True, primary_key=True)
    set_name = Column(String, nullable=False)
    string_maker_id = Column(Integer,ForeignKey('string_maker.id'), nullable=False)

    __table_args__ = (UniqueConstraint('set_name', 'string_maker_id', name='maker_set_1'),)

    def __init__(self, set_name=None, string_maker_id=None):
        self.set_name = set_name
        self.string_maker_id = string_maker_id

    def __repr__(self):
        return "<StringSet(set name='%s')>" % (self.set_name)

class StringType(Base):
    __tablename__ = 'string_type'
    
    id = Column(Integer, primary_key=True)
    string_type_name = Column(String, nullable=True)

    def __repr__(self):
        return "<StringType(string type name='%s')>" % (self.string_type_name)

    
class ModelSchema(Schema):
    id = fields.Int()
    name = fields.Str()

class MakerSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    models = fields.Nested(ModelSchema, many=True)


model_schema = ModelSchema(many=True)
maker_schema = MakerSchema()


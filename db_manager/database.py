from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# engine = create_engine('sqlite:////tmp/test.db', convert_unicode=True)
engine = create_engine('postgresql://dojoman:dojopass@localhost/guitarzone')
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Session = sessionmaker(bind=engine)

# session = Session()

Base = declarative_base()
Base.query = db_session.query_property()

def init_db(extend_existing=True):
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import guitar.models
    Base.metadata.create_all(bind=engine)
